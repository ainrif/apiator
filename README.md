# Apiator 

Simple library for auto-documenting your java/groovy rest-api

|               |                       Stable                      |                    develop-SNAPSHOT                    |
|---------------|:-------------------------------------------------:|:------------------------------------------------------:|
| Artifact repo |       [![Download][artifact_img]][artifact]       | [![Develop][artifact_img_snapshot]][artifact_snapshot] |
| Build status  |                         -                         |   [![Build Status][build_img_develop]][build_develop]  |

  [artifact_img]: https://api.bintray.com/packages/ainrif/maven/apiator/images/download.svg
  [artifact]: https://bintray.com/ainrif/maven/apiator/_latestVersion
  
  [artifact_img_snapshot]: https://img.shields.io/badge/JitPack-develop-blue.svg
  [artifact_snapshot]: https://jitpack.io/#org.bitbucket.ainrif/apiator/develop-SNAPSHOT
  [build_img_develop]: https://semaphoreci.com/api/v1/ainrif/apiator/branches/develop/shields_badge.svg
  [build_develop]: https://semaphoreci.com/ainrif/apiator/branches/develop

## Getting started

You can find a draft of documentation [here](https://apiator.ainrif.com/) or look into SmokeSpec.groovy

## How you can help

* If you find some bugs please create an issue [here](https://bitbucket.org/ainrif/apiator/issues)
* If you want to suggest feature also create an [issue](https://bitbucket.org/ainrif/apiator/issues)
* Or make a pool request with your feature
* The repo mirror also available on [GitHub](https://github.com/katoquro/apiator)